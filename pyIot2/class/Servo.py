import RPi.GPIO as GPIO
import time

class ServoMoter():
    def __init__(self):
        pass

    def start(self):
        pass

    def move(self):
        pass

    def stop(self):
        pass

    def cleanup(self):
        pass

    @property
    def gp_out(self):
        return self._gp_out
    @gp_out.setter()
    def gp_out(self, value):
        self._gp_out = value
    @gp_out.deleter
    def gp_out(self):
        del self._gp_out